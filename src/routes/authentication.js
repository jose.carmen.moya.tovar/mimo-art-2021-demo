const express = require('express');
const router = express.Router();
const nodemailer = require('nodemailer');
const pool = require('../database');

const passport = require('passport');
const { isLoggedIn } = require('../lib/auth');

// SIGNUP
router.get('/signup', (req, res) => {
  res.render('auth/signup');
});

router.post('/signup', passport.authenticate('local.signup', {
  successRedirect: '/',
  failureRedirect: '/signup',
  failureFlash: true
}));

// SINGIN
router.get('/signin', (req, res) => {
  res.render('auth/signin');
});

router.post('/signin', (req, res, next) => {
  req.check('username', 'Usuario requerido').notEmpty();
  req.check('password', 'Contraseña requerida').notEmpty();
  const errors = req.validationErrors();
  if (errors.length > 0) {
    req.flash('message', errors[0].msg);
    res.redirect('/signin');
  }
  passport.authenticate('local.signin', {
    successRedirect: '/',
    failureRedirect: '/signin',
    failureFlash: true
  })(req, res, next);
});

// SINGIN_ADMIN
router.get('/signin_admin', (req, res) => {
  res.render('auth/signin_admin');
});

router.post('/signin_admin', (req, res, next) => {
  req.check('username', 'Usuario requerido').notEmpty();
  req.check('password', 'Contraseña requerida').notEmpty();
  const errors = req.validationErrors();
  if (errors.length > 0) {
    req.flash('message', errors[0].msg);
    res.redirect('/signin_admin');
  }
  passport.authenticate('local.signin_admin', {
    successRedirect: '/adminpanel',
    failureRedirect: '/signin_admin',
    failureFlash: true
  })(req, res, next);
});

// RECORDAR PASSWORD
router.get('/recordarpass', (req, res) => {
  res.render('auth/recordarpass');
});


//SEND EMAIL
router.post('/sendpass', (req, res, next) => {

  const email = req.body.email;


  const output = `
  <!DOCTYPE>
  <html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style type="text/css">
    body {margin: 0; padding: 0; min-width: 100%!important;}
    img {height: auto;}
    .content {width: 100%; max-width: 600px;}
    .header {padding: 40px 30px 20px 30px;}
    .innerpadding {padding: 30px 30px 30px 30px;}
    .borderbottom {border-bottom: 1px solid #05AFF2;}
    .subhead {font-size: 15px; color: #ffffff; font-family: sans-serif; letter-spacing: 10px;}
    .h1, .h2, .bodycopy {color: #153643; font-family: sans-serif;}
    .h1 {font-size: 33px; line-height: 38px; font-weight: bold;}
    .h2 {padding: 0 0 15px 0; font-size: 24px; line-height: 28px; font-weight: bold;}
    .bodycopy {font-size: 16px; line-height: 22px;}
    .button {text-align: center; font-size: 18px; font-family: sans-serif; font-weight: bold; padding: 0 30px 0 30px;}
    .button a {color: #ffffff; text-decoration: none;}
    .footer {padding: 20px 30px 15px 30px;}
    .footercopy {font-family: sans-serif; font-size: 14px; color: #ffffff;}
    .footercopy a {color: #ffffff; text-decoration: underline;}
    
    @media only screen and (max-width: 550px), screen and (max-device-width: 550px) {
    body[yahoo] .hide {display: none!important;}
    body[yahoo] .buttonwrapper {background-color: transparent!important;}
    body[yahoo] .button {padding: 0px!important;}
    body[yahoo] .button a {background-color: #e05443; padding: 15px 15px 13px!important;}
    body[yahoo] .unsubscribe {display: block; margin-top: 20px; padding: 10px 50px; background: #2f3942; border-radius: 5px; text-decoration: none!important; font-weight: bold;}
    }
    </style>
  </head>
  
  <body yahoo bgcolor="#f1ede6">
  <table width="100%" bgcolor="#f1ede6" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td>   
      <table bgcolor="#ffffff" class="content" align="center" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <td bgcolor="#e09100" class="header">
  
            <table class="col425" align="left" border="0" cellpadding="0" cellspacing="0" style="width: 100%; max-width: 425px; color=#ffffff">  
              <tr>
               <tr>
                <td height="70">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td class="subhead" style="padding: 0 0 0 3px; color: #ffffff">
                        URGENTE
                      </td>
                    </tr>
                    <tr>
                      <td class="h1" style="padding: 5px 0 0 0; color: #ffffff">
                        RECORDAR  CONTRASEÑA
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td class="innerpadding borderbottom">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td class="h2">
                  Hola: ${email} 
                </td>
              </tr>
              <tr>
          <td class="h2">
                   Tu contraseña es:  ${email}
          </td>
              </tr>
            </table>
          </td>
        </tr>
  
        </tr>
        <tr>
          <td class="footer" bgcolor="#e09100">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center" class="footercopy">				
          <div class="copyright">Copyright &copy;<script>document.write(new Date().getFullYear());</script>Todos los derechos reservados | MiMo art <i class="fa fa-heart-o" aria-hidden="true"></i></div>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      </td>
    </tr>
  </table>
  </body>
  </html>
  
  `;


  let transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
      port: 465,
      secure: true,
    auth: {
        user: 'mimoutng@gmail.com',
        pass: 'Playstation1$'
    },
    tls:{
      rejectUnauthorized:false
    }
  });


  let mailOptions = {
      from: 'mimoutng@gmail.com', 
      to: req.body.email,
      subject: 'Recordar contraseña',
      text: '', 
      html: output 
  };

  transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
          return console.log(error);
      }
      console.log('Message sent: %s', info.messageId);   
      console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

      res.render('auth/signin');
  });
  });


// LOGOUT
router.get('/logout', (req, res) => {
  req.logOut();
  res.redirect('/');
});

router.get('/adminpanel', isLoggedIn, (req, res) => {
  res.render('adminpanel');
});

router.get('/signin_admin', (req, res) => {
  res.render('signin_admin');
});


module.exports = router;
