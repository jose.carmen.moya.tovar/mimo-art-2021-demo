const express = require('express');
const router = express.Router();

const passport = require('passport');

router.get('/', async (req, res) => {
    res.render('index');
});

router.get('/carrito', (req, res) => {
    res.render('carrito');
  });
  
router.get('/catalogo', (req, res) => {
    res.render('catalogo');
  });
  
router.get('/checkout', (req, res) => {
    res.render('checkout');
  });
  
router.get('/contacto', (req, res) => {
    res.render('contacto');
  });
  
router.get('/nosotros', (req, res) => {
    res.render('nosotros');
  });
  
router.get('/preguntas', (req, res) => {
    res.render('preguntas');
  });
  
router.get('/product', (req, res) => {
    res.render('product');
  });

module.exports = router;