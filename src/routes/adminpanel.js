const express = require('express');
const router = express.Router();

const passport = require('passport');

const frasesController = require('../controllers/frasesController');
const museosController = require('../controllers/museosController');
const usuariosController = require('../controllers/usuariosController');  

// ADMIN PANEL
  router.get('/adminpanel', (req, res) => {
    res.render('adminpanel/adminpanel');
  });
  
  // Usuarios
  router.get('/museos', museosController.list);
  router.post('/addMuseo', museosController.save);
  router.get('/updateMuseo/:id', museosController.edit);
  router.post('/updateMuseo/:id', museosController.update);
  router.get('/deleteMuseo/:id', museosController.delete);
  
  // Frases
  router.get('/frases', frasesController.list);
  router.post('/addFrases', frasesController.save);
  router.get('/updateFrases/:id', frasesController.edit);
  router.post('/updateFrases/:id', frasesController.update);
  router.get('/deleteFrases/:id', frasesController.delete);
  
  // Usuarios
  router.get('/usuarios', usuariosController.list);
  

module.exports = router;