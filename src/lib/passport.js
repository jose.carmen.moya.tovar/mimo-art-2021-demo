const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const pool = require('../database');
const helpers = require('./helpers');

passport.use('local.signin', new LocalStrategy({
  usernameField: 'username',
  passwordField: 'password',
  passReqToCallback: true
}, async (req, username, password, done) => {
  const rows = await pool.query('SELECT * FROM users WHERE rol = 0 AND username = ?', [username]);
  if (rows.length > 0) {
    const user = rows[0];
    const validPassword = await helpers.matchPassword(password, user.password)
    if (validPassword) {
      done(null, user, req.flash('success', 'Bienvenido ' + user.username + ' a nuestro sitio web ofial MiMo art'));
    } else {
      done(null, false, req.flash('message', 'Contraseña Incorrecta'));
    }
    } else {
      done(null, false, req.flash('message', 'El nombre del usuario no existe.'));
    }
}));

passport.use('local.signin_admin', new LocalStrategy({
  usernameField: 'username',
  passwordField: 'password',
  passReqToCallback: true
}, async (req, username, password, done) => {
  const rows = await pool.query('SELECT * FROM users WHERE rol = 1 AND username = ?', [username]);
  if (rows.length > 0) {
    const user = rows[0];
    const validPassword = await helpers.matchPassword(password, user.password)
    if (validPassword) {
      done(null, user, req.flash('success', 'Bienvenido ' + user.username + ' a nuestro sitio web ofial MiMo art'));
    } else {
      done(null, false, req.flash('message', 'Contraseña Incorrecta'));
    }
    } else {
      done(null, false, req.flash('message', 'El nombre del usuario no existe.'));
    }
}));


passport.use('local.signup', new LocalStrategy({
  usernameField: 'username',
  passwordField: 'password',
  passReqToCallback: true
}, async (req, username, password, done) => {

  const { nombre } = req.body;
  const { ap_paterno } = req.body;
  const { ap_materno } = req.body;
  const { email } = req.body;
  const { rol } = req.body;

  let newUser = {
    nombre,
    ap_paterno,
    ap_materno,
    email,
    username,
    password,
    rol
  };
  newUser.password = await helpers.encryptPassword(password);
  // Saving in the Database
  const result = await pool.query('INSERT INTO users SET ? ', newUser);
  newUser.id = result.insertId;
  return done(null, newUser);
}));

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser(async (id, done) => {
  const rows = await pool.query('SELECT * FROM users WHERE id = ?', [id]);
  done(null, rows[0]);
});

