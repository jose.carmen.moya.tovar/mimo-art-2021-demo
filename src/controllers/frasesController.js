const frasesController = {};

const pool = require('../database');

    frasesController.list = (req, res) => {
        
        pool.query('SELECT * FROM frases',(err,datos)=>{
            res.render('adminpanel/frases',{
                data : datos,
            });
        });
    };


    frasesController.save = (req, res) => {
        const data = req.body;
            pool.query('INSERT INTO frases set ?', data, (err, frases) => {
                console.log(frases)
                res.redirect('/frases');
            });
    };

    frasesController.edit = (req, res) => {
    const { id } = req.params;
        pool.query("SELECT * FROM frases WHERE id = ?", [id], (err, rows) => {
        res.render('adminpanel/frases_edit', {
            data: rows[0]
        })
        });
    };

    frasesController.update = (req, res) => {
    const { id } = req.params;
    const newCustomer = req.body;

    pool.query('UPDATE frases set ? where id = ?', [newCustomer, id], (err, rows) => {
        res.redirect('/frases');
    });
    };

    frasesController.delete = (req, res) => {
        const { id } = req.params;
        pool.query('DELETE FROM frases WHERE id = ?', [id], (err, rows) => {
            res.redirect('/frases');
        });
    };

module.exports = frasesController;
