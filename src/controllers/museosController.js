const museosController = {};

const pool = require('../database');

    museosController.list = (req, res) => {
        
        pool.query('SELECT * FROM museos',(err,datos)=>{
            res.render('adminpanel/museos',{
                data : datos,
            });
        });
    };


    museosController.save = (req, res) => {
        const data = req.body;
            pool.query('INSERT INTO museos set ?', data, (err, museos) => {
                console.log(museos)
                res.redirect('/museos');
            });
    };

    museosController.edit = (req, res) => {
        const { id } = req.params;
            pool.query("SELECT * FROM museos WHERE id = ?", [id], (err, rows) => {
            res.render('adminpanel/museos_edit', {
                data: rows[0]
            })
            });
        };
    
    museosController.update = (req, res) => {
        const { id } = req.params;
        const newCustomer = req.body;
    
        pool.query('UPDATE museos set ? where id = ?', [newCustomer, id], (err, rows) => {
            res.redirect('/museos');
        });
        };

    museosController.delete = (req, res) => {
        const { id } = req.params;
        pool.query('DELETE FROM museos WHERE id = ?', [id], (err, rows) => {
            res.redirect('/museos');
        });
    };

module.exports = museosController;
